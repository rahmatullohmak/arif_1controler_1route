<?php
use App\Http\Controllers\tescontroler;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\DosenController;
use App\Http\Controllers\admin;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

// Route::middleware('auth')->group(function () {
//     Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//     Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
//     Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
// });
// Route::get('/tes', function () {
//     return view('index');
// });
// Route::post('/rt', [DosenController::class, 'welcom']);
// Route::get('/home', [DosenController::class, 'index']);
// Route::get('/eee', [tescontroler::class, 'index']);
// Route::get('/tess', [tes::class, 'tes'])->name('profile.edit');
// Route::get('/rdt', function () {
//     return view('welcom');
// });
// require __DIR__.'/auth.php';
Route::post('/rt', [DosenController::class, 'welcom']);
Route::get('/home', [DosenController::class, 'index']);
Route::get('/ibu_ismi', function () {
    return view('tugas_ibuismi.master.master');
});
Route::get('/table', [admin::class, 'index']);
Route::get('/tables', [admin::class, 'tables']);
